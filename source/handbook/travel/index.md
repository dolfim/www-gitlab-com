---
layout: markdown_page
title: "Travel"
---

## Arranging your company travel

The company will cover all work related travel costs.  
- Understand that the guidelines of [Spending Company Money](https://about.gitlab.com/handbook/#spending-company-money) still apply
- The company can accommodate your custom request. It is OK to stay longer on 
your trip. However, the extra days will not be covered by the company.

## Specific Topics Relating to Egencia and Expenses

- [Setting up your Egencia account](#setup-egencia)  
- [Booking travel through Egencia](#egencia)  
- [Expenses while traveling](#expenses-while-traveling)


### Setting up your Egencia account<a name="setup-egencia"></a>

Below is some more information to help you get set up with your [Egencia](https://www.egencia.com) account.  

1. You will receive an email from Egencia with your username.
1. With the link, from step one in this email, you can set a password with link.
1. Once you are in your dashboard, you can setup preferences in your travel profile. 
You can enter your profile in the top right corner. Make sure to add your phone number.
1. When you add your passport info to your profile this will populate your info directly when booking a flight.
1. Now let's start booking!

### Booking travel through [Egencia](https://www.egencia.com) <a name="egencia"></a>

If you book through Egencia, the costs will be added to the GitLab invoice and **no credit card is needed**.

1. Login to your account. If you've not yet set up your account, do this first.
1. The dashboard gives you directly the option to start booking. The flight option
is displayed,other options are booking a train ticket, hotel or car
1. Input info for the flight you want to book.
1. Select your favorite option for the outbound flight. Please note that our policy 
is that you can book up to $250 of the cheapest flight option. Booking more than 
$250 over that amount will flag "out of policy" and requires approval.
1. Select your return (inbound) flight
1. Read & check the box "I have read and accept the rules and restrictions". Now 
you have the possibility to "continue to checkout" or "add more items to this trip" 
such as another flight, a hotel reservation or booking a car.
1. Select "continue to checkout" if you just want to book the flight.

#### Checkout flight only

1. Enter your passport information in the "Secure flight" window
1. Enter your seat preferences and frequent flyer program info if wanted
1. Enter your reason for booking, this is required for every booking

#### Continue adding a hotel or car

Under "add more items to this trip" you can select what you want to add.

1. The platform auto populates your info based on your flight.
1. For adding a **hotel**:
  - You will get a window including a map with all hotel options.
  - On the top right you can filter based upon preferences
  - Click on a hotel to get more information
  - To go back to the overview of hotels click on X in the top right corner
  - To choose the hotel room click on the price
  - When choosing the hotel you will go to an overview of the costs and hotel info
1. For adding a **car**:
  - you will get a window with your car options
  - On the top of the window you can sort on price
  - to select a car click "select"
  - When choosing the car you will go to an overview of the costs and rules/restrictions info
Now you once again have the possibility to "continue to checkout" or "add more items to this trip" such as another flight

#### Checkout flight and hotel and/or a car

You will get a total overview of what is booked. You can see that the hotel and/or car are awaiting confirmation of the third party that will be booked once you checkout.

#### Options in Egencia

* In stead of choosing "checkout" you can select "safe trip and view itinerary"
* Here you will get an overview of everything that was selected and you can change items, such as your flight. 
* In the menu on the top left you have some options like "invite others", "print itinerary", send or delete.
* You can also download the app "Egencia TripNavigator" to keep your itinerary close and even do bookings on the go if needed.

Please send a message to travel@gitlab.com to get help in booking your travel.

### Expenses While Traveling <a name="expenses-while-traveling"></a>

1. The company will pay for lodging and meals during the part of the trip
that is work related. Depending on the distance of your travel, this can include
one day before and one day after the work related business. For example, if you
are attending a 3 day conference in a jetlag-inducing location, the company will
cover your lodging and meals those 3 days as well as one day before and one day after.
1. Meals and lodging are covered. The "spend it like it is your own money" rule applies here except 
if explicitly noted otherwise. For example, the limits for during the Austin Summit
are detailed on the [Austin Summit page](https://dev.gitlab.org/summit_group_2016/Austin-Summit-2016-project).
1. Always bring a credit card with you when travelling for company business if you have one.
1. Hotels will expect you to have a physical credit card to present upon check-in. 
This credit card will be kept on file for the duration of your stay. Even if your lodging 
was pre-paid by the company or by using a company credit card, the Hotel may still
require a card to be on file for "incidentals".
1. If you incur any work-travel related expenses (on your personal card or a GitLab 
company card), please make sure to save the original receipt. 
1. When your trip is complete, please file an expense report via Expensify. 
Expensify steps can be found http://help.expensify.com/getting-started/ 

See the section on [Spending Company Money](https://about.gitlab.com/handbook/#spending-company-money)
on the handbook for more details on the expense policies.
