---
layout: markdown_page
title: "Content Marketing"
---

## Introduction

The philosophy for Content Marketing at GitLab is to help bring insider
knowledge and implicit practice to the hands of developers getting acquainted
with GitLab, and decision makers considering GitLab.

We want to meet each user where they are now, and help them be the most
efficient they can be with our tools.
We want to help their teams realize their creative and collaborative goals.

We also want to address the whole organization, the coal-face of development
or in decision making roles. GitLab is not only used for code development and
review. Those same tools are used by team members to track progress, create
documentation and collaborate on projects. When we show how we work "inside
GitLab" we also model how to use our software.

## 2016 activities

- Publish an active [blog](blog/) with useful content relevant to GitLab users.
- Host [webcasts](webcasts/)
which welcome newcomers, celebrate with existing users, and provide access to expertise.
- Publish a twice-monthly email newsletter you can sign up to on [our contact page](https://about.gitlab.com/contact/).
