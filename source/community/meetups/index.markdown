---
layout: markdown_page
title: "GitLab Meetups"
comments: false
sharing: true
suppress_header: true
---

## Want to share your enthusiasm for GitLab?

We know people in our community would love to get together to
meet and talk about using GitLab, and all the ways you collaborate on code.

It's great that Gitlab users all write different kinds of applications,
you work within other software communities, yet you all share the experience
of collaborating, communicating and deploying with GitLab.

### Join GitLab Meetup groups

- [San Francisco - GitLab Meetup Group - ](http://www.meetup.com/GitLab-Meetup-Group/)
- [Dutch - GitLab Meetup Group](http://www.meetup.com/Dutch-GitLab-Meetup-Group/)

If you'd like to help us connect with the community, tell us if you would like to:

- Become a GitLab Meet-up Organizer
- Or - Host a GitLab-themed meet-up within your own software community

Email community at GitLab.com and include "Meetup" in the subject line.

## Meetup in a box!

You might be an experienced open source contributor, or maybe you're
a first timer. If you'd like to lead a meet-up in your local area we're offering you
"Meet-up in a box" to make that easier.

### What we will offer

This sponsorship package will make it easier for you organize a local event.

- We'll supply the snacks/pizza and drinks (and venue if needed.)
- Presentation materials to talk about the latest release and community news.
- Nifty stickers for attendees.
- For GitLab Meet-up organizers, you can get a t-shirt.

### What we ask of you

- Tell us about your event so we can help promote it.
- Help share feedback and ideas about how to improve Meetup-in-a-box.
- Be nice to each other. Follow our community [code of conduct](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#code-of-conduct).
